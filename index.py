from icecream import *
import streamlit as st
import pandas as pd
import numpy as np
from datetime import datetime
from random import choice
import time
from PIL import Image
image = Image.open('tozDXyW.png')

st.image(image)

emojis = [
    ":butterfly:",
    ":ant:",
    ":bug:",
    ":lady_beetle:",
    ":spider:",
    ":scorpion:",
    ":mosquito:"
]

TEMPERATURA_DE_BASE_MARIPOSA = 10.9
TEMPERATURA_DE_BASE_OVO = 9.3
CONSTANTE_TERMICA_DA_MARIPOSA = 463
CONSTANTE_TERMICA_DO_OVO = 44.2
CONSTANTE_TERMICA_DA_LAGARTA = 297.2


@st.experimental_memo
def convert_df(df):
    return df.to_csv(
        index=False,
        header=False
    ).encode('utf-8')


def handleDate1(date: str):
    if len(date) == 10:
        return date
    else:
        day = date.split('/')[1]
        month = date.split('/')[0]
        year = date.split('/')[2]

        if len(day) == 2 and len(month) == 2:
            return '{}/{}/{}'.format(day, month, year)
        elif len(day) == 1 and len(month) == 2:
            return '0{}/{}/{}'.format(day, month, year)
        elif len(day) == 2 and len(month) == 1:
            return '{}/0{}/{}'.format(day, month, year)
        elif len(day) == 1 and len(month) == 1:
            return '0{}/0{}/{}'.format(day, month, year)


def handleDate2(date: str):
    day = date.split('/')[0]
    month = date.split('/')[1]
    year = date.split('/')[2]

    return '{}/{}/20{}'.format(day, month, year)


def handleDate3(dateStr: str):
    date_str = '{}-{}-{}'.format(
        dateStr.split('/')[0],
        dateStr.split('/')[1],
        dateStr.split('/')[2]
    )

    return datetime.strptime(date_str, '%d-%m-%Y').date()


def handleDate4(dateStr: str):
    date_str = '{}/{}/{}'.format(
        dateStr.split('-')[2],
        dateStr.split('-')[1],
        dateStr.split('-')[0]
    )

    return date_str


try:
    st.header("Junção de dados")
    uploaded_file = st.file_uploader(
        "Dados de monitoramento convencional",
        type=['csv'],
    )

    if uploaded_file is not None:
        st.write("Processamento dos dados de monitoramento...")

    new_list = []
    if uploaded_file is not None:
        initial_dataframe = pd.read_csv(uploaded_file)

        del initial_dataframe["Fazenda"]
        del initial_dataframe["Região 0"]
        del initial_dataframe["Severidade"]
        del initial_dataframe["Categoria"]
        del initial_dataframe["Unidade"]
        del initial_dataframe["Pontos Amostrais"]
        del initial_dataframe["Nível de Controle"]
        del initial_dataframe["Área do Talhão"]
        del initial_dataframe["Nível de Dano"]
        del initial_dataframe["Dia de Emergência"]
        del initial_dataframe["Cultura"]
        del initial_dataframe["Cultivar"]

        st.write("1. Deleção de colunas :white_check_mark:")

        options = [
            "Porcentagem de pontos com presença de ovos",
            "Porcentagem de pontos com presença ovos",
            "Média de lagartas até 0,5 cm",
            "Média de lagartas de 0,5 a 1,0 cm",
            "Média de lagartas maiores que 1,0 cm"
        ]

        dataframe = initial_dataframe[initial_dataframe["Indicador"].isin(
            options)]

        st.write("2. Filtragem de indicadores :white_check_mark:")

        options = [
            "Lagarta-militar",
            "Helicoverpa armigera",
        ]

        dataframe = dataframe[dataframe["Ocorrência"].isin(options)]

        st.write("3. Filtragem de ocorrências :white_check_mark:")

        resultado_do_indicador = dataframe[[
            "Indicador", "Resultado do Indicador"]].values.tolist()

        for item in resultado_do_indicador:
            item[1] = float(item[1].replace(',', '.'))
            if item[0] == 'Média de lagartas de 0,5 a 1,0 cm':
                item[0] = 'M1X'
            elif item[0] == 'Média de lagartas maiores que 1,0 cm':
                item[0] = 'M2X'
            elif item[0] == 'Média de lagartas até 0,5 cm':
                item[0] = 'M3X'
            else:
                item[0] = 'P4X'

        if resultado_do_indicador[-1][0] == 'M1X':
            resultado_do_indicador.append(['M2X', 0.0])
            resultado_do_indicador.append(['M3X', 0.0])
            resultado_do_indicador.append(['P4X', 0.0])
        elif resultado_do_indicador[-1][0] == 'M2X':
            resultado_do_indicador.append(['M3X', 0.0])
            resultado_do_indicador.append(['P4X', 0.0])
        elif resultado_do_indicador[-1][0] == 'M3X':
            resultado_do_indicador.append(['P4X', 0.0])
        else:
            pass

        aux_list = []

        for item in resultado_do_indicador:
            aux_list.append(item[0] + str(item[1]))

        for i in range(len(aux_list)):
            if 'M1' in aux_list[i]:
                aux_list[i] = str(16666) + aux_list[i][3:]
            elif 'M2' in aux_list[i]:
                aux_list[i] = str(26666) + aux_list[i][3:]
            elif 'M3' in aux_list[i]:
                aux_list[i] = str(36666) + aux_list[i][3:]
            elif 'P4' in aux_list[i]:
                aux_list[i] = str(46666) + aux_list[i][3:]

        indexes = []
        for i in range(len(aux_list)):
            if '36666' in aux_list[i] and '46666' not in aux_list[i + 1]:
                indexes.append(i + 1)

        indexes = tuple(indexes)

        arr = np.array(aux_list)

        chars = []
        for item in indexes:
            chars.append(46666)

        new_arr = np.insert(arr, indexes, tuple(chars))

        for i in range(len(new_arr)):
            new_arr[i] = new_arr[i].replace('6666', ' ')

        arr_list = new_arr.tolist()

        arr_sublist = [arr_list[i:i+4] for i in range(0, len(arr_list), 4)]

        for i in range(len(arr_sublist)):
            for j in range(len(arr_sublist[i])):
                arr_sublist[i][j] = arr_sublist[i][j][2:]
                if arr_sublist[i][j] == '':
                    arr_sublist[i][j] = 0.0

                arr_sublist[i][j] = float(arr_sublist[i][j])

        df_indicadores = pd.DataFrame(arr_sublist)

        header = [
            "Média de lagartas de 0,5 a 1,0 cm",
            "Média de lagartas maiores que 1,0 cm",
            "Média de lagartas até 0,5 cm",
            "Porcentagem de pontos com presença de ovos"
        ]

        df_indicadores.columns = header

        st.write("4. Transposição dos dados de indicadores :white_check_mark:")

        del dataframe["Resultado do Indicador"]

        df_list = dataframe.values.tolist()

        for i in range(len(df_list)):
            df_list[i] = str(df_list[i])

        for i in range(len(df_list)):
            df_list[i] = df_list[i].replace(', ', '***')[1:-1]

        arr = tuple(df_list)
        chars = []
        for item in indexes:
            chars.append('random string')

        new_arr = np.insert(arr, indexes, tuple(chars))

        aux_list = []
        for i in range(0, len(new_arr.tolist()), 4):
            aux_list.append(new_arr.tolist()[i])

        for i in range(len(aux_list)):
            aux_list[i] = aux_list[i].split('***')

        for i in range(len(aux_list)):
            for j in range(len(aux_list[i])):
                aux_list[i][j] = aux_list[i][j][1:-1]

        dataframe = pd.DataFrame(aux_list)

        final = pd.concat([dataframe, df_indicadores], axis=1, join='inner')

        final = final.drop(final.columns[[3]], axis=1)

        final_list = final.values.tolist()
        for item in final_list:
            item[0] = handleDate1(item[0])
            item[2] = int(item[2].split('-')[0])
            if item[3] == 'Lagarta-militar':
                item[3] = "Spodoptera frugiperda"

        st.write("5. Troca de nome da Lagarta-militar :white_check_mark:")

        dataframe_monitoramento_convencional = pd.DataFrame(
            final_list).iloc[:, [0, 2, 3, 1, 4, 5, 6, 7]]

        st.write("1. Mudança de ordem das colunas :white_check_mark:")

        uploaded_file = st.file_uploader(
            "Dados do Backoffice",
            type=['csv'],
            key=""
        )
        if uploaded_file is not None:
            dataframe = pd.read_csv(uploaded_file)
            traps_dataframe = dataframe

            st.write("Processamento dos dados de armadilhas...")

            del dataframe["Gleba"]
            del dataframe["Safra"]
            del dataframe["Fazenda"]
            del dataframe["Cultura"]

            st.write("1. Deleção de colunas :white_check_mark:")

            df_list = dataframe.values.tolist()

            for item in df_list:
                for subitem in item[5].split():
                    if subitem.isdigit():
                        item[5] = int(subitem)

            for item in df_list:
                for subitem in item[1].split():
                    if subitem.isdigit():
                        item[1] = int(subitem)

            for item in df_list:
                split_item = item[2].split()
                item[2] = split_item[0] + ' ' + split_item[1].lower()
                item[0] = handleDate2(item[0])

            st.write(
                "2. Tratamento e uniformização das datas e nomes de espécies :white_check_mark:")
            armadilhas = df_list

    ###############################################################################

    aux_list = []

    # Laços de repetição para adicionar as 3 primeiras colunas (Semelhantes)
    # na lista auxiliar aux_list
    for item in armadilhas:
        aux_list.append([
            item[0],
            item[1],
            item[2]
        ])

    monitoramento_convencional = dataframe_monitoramento_convencional.values.tolist()
    for item in monitoramento_convencional:
        aux_list.append([
            item[0],
            item[1],
            item[2]
        ])

    # Ordena a aux_list com base na coluna de índice 1 (Coluna da áreas)

    aux_list.sort(key=lambda x: x[1])

    aux_list2 = []

    for item in aux_list:
        if item not in aux_list2:
            aux_list2.append(item)

    st.write("3. Adição de todas as áreas :white_check_mark:")

    for i in range(len(aux_list2)):
        for j in range(len(armadilhas)):
            if armadilhas[j][0] == aux_list2[i][0] and armadilhas[j][1] == aux_list2[i][1] and armadilhas[j][2] == aux_list2[i][2]:
                aux_list2[i].append(armadilhas[j][3])  # Nível de controle
                aux_list2[i].append(armadilhas[j][4])  # Área
                aux_list2[i].append(armadilhas[j][5])  # Coordenada
                aux_list2[i].append(armadilhas[j][6])  # Ocorrência

    st.write("4. [f] Dados das armadilhas :white_check_mark:")
    # Solução para o bug de adicionar mais itens na sublista
    aux_list3 = []
    for item in aux_list2:
        aux_list3.append(item[:7])

    # Verificar qual o maior comprimento de sublista para adicionar o espaço vazio
    # na linha que não teve adição de dados

    lens = []
    for item in aux_list3:
        lens.append(len(item))

    for item in aux_list3:
        if len(item) < max(lens):
            item.append('')
            item.append('')
            item.append('')
            item.append('')

    for j in range(len(monitoramento_convencional)):
        for i in range(len(aux_list3)):
            if monitoramento_convencional[j][0] == aux_list3[i][0] and monitoramento_convencional[j][1] == aux_list3[i][1] and monitoramento_convencional[j][2] == aux_list3[i][2]:
                aux_list3[i].append(monitoramento_convencional[j][3])
                aux_list3[i].append(monitoramento_convencional[j][4])
                aux_list3[i].append(monitoramento_convencional[j][5])
                aux_list3[i].append(monitoramento_convencional[j][6])
                aux_list3[i].append(monitoramento_convencional[j][7])

    st.write("4. [f] Dados do monitoramento convencional :white_check_mark:")

    aux_list4 = []
    for item in aux_list3:
        aux_list4.append(item[:12])

    for item in aux_list4:
        item[0] = handleDate3(item[0])

    st.write("5. Formatação da data :white_check_mark:")

    lens = []
    for item in aux_list4:
        lens.append(len(item))

    for item in aux_list4:
        if len(item) < max(lens):
            item.append('')
            item.append('')
            item.append('')
            item.append('')
            item.append('')

    for item in aux_list4:
        item[0] = str(item[0])

    # Indices das linhas cujas as áreas não estão no backoffice
    indexes = []
    for item in aux_list4:
        if item[3] != 7:
            indexes.append(aux_list4.index(item))

    datas = []
    areas = []
    for item in aux_list4:
        if item[0] not in datas:
            datas.append(item[0])
        if item[1] not in areas:
            areas.append(item[1])

    dataframe = pd.DataFrame(aux_list4)

    dataframe = dataframe.drop(index=indexes)

    aux_list5 = dataframe.values.tolist()
    datas = []
    areas = []
    for item in aux_list5:
        if item[0] not in datas:
            datas.append(item[0])
        if item[1] not in areas:
            areas.append(item[1])

    # ataframe = dataframe[dataframe['Nível de Controle'].isin(7)]

    dataframe = dataframe.drop(dataframe.columns[[5]], axis=1)

    headers = [
        "Data",
        "Área",
        "Ocorrência",
        "Nível de Controle",
        "Capturas",
        "Localização",
        "Fazenda",
        "Média de lagartas até 0,5 cm",
        "Média de lagartas de 0,5 a 1,0 cm",
        "Média de lagartas maiores que 1,0 cm",
        "Porcentagem de pontos com presença de ovos"
    ]

    dataframe.columns = headers

    dataframe.sort_values(by='Data', ascending=True, inplace=True)

    col1, col2, col3 = st.columns(3)

    ocorrencia = st.multiselect(
        'Ocorrência',
        ['Spodoptera frugiperda', 'Helicoverpa armigera'],
        ['Spodoptera frugiperda']
    )

    area = st.multiselect(
        'Área',
        areas,
        [423]
    )

    data = st.multiselect(
        'Data',
        datas,
        ['2023-01-23']
    )

    dataframe = dataframe[dataframe['Ocorrência'].isin(ocorrencia)]
    dataframe = dataframe[dataframe['Área'].isin(area)]
    dataframe = dataframe[dataframe['Data'].isin(data)]

    df_output = dataframe.values.tolist()
    df_list = traps_dataframe.values.tolist()

    talhoes = []
    traps = []
    for item in df_list:
        if item[1] not in talhoes:
            talhoes.append(item[1])
        if item[5] not in traps:
            traps.append(item[5])

    for i in range(len(df_output)):
        for j in range(len(talhoes)):
            if str(df_output[i][1]) in talhoes[j]:
                df_output[i].append(talhoes[j])

    for i in range(len(df_output)):
        for j in range(len(traps)):
            if str(df_output[i][1]) in traps[j]:
                df_output[i].append(traps[j])

    for item in df_output:
        item[0] = handleDate4(item[0])

    for i in range(len(df_output)):
        if 'Prata' in df_output[i][12]:
            df_output[i].append('Prata I')
        elif 'Santa Maria' in df_output[i][12]:
            df_output[i].append('Santa Maria I')
        elif 'Jatobá' in df_output[i][12]:
            df_output[i].append('Jatobá')
        elif 'Santa Brigida' in df_output[i][12]:
            df_output[i].append('Santa Brigida')
        else:
            df_output[i].append('?')

    dataframe = pd.DataFrame(df_output)

    dataframe = dataframe.drop(dataframe.columns[[1, 6]], axis=1)

    dataframe = dataframe.reindex(
        columns=[0, 13, 2, 11, 5, 12, 4, 7, 10, 8, 9, 3])

    header = [
        "Data",
        "Fazenda",
        "Ocorrência",
        "Talhão",
        "Localização",
        "Armadilha",
        "Capturas",
        "Porcentagem de pontos com presença de ovos",
        "Média de lagartas até 0,5 cm",
        "Média de lagartas de 0,5 a 1 cm",
        "Média de lagartas maiores que 1 cm",
        "Nível de Controle"
    ]

    dataframe.columns = header

    if st.button('Gerar'):
        with st.spinner('Aguarde...'):
            time.sleep(.5)
        st.write(dataframe)
        csv = convert_df(dataframe)
        st.download_button(
            "Download {}".format(choice(emojis)),
            csv,
            "compilado.csv",
            "text/csv",
            key='download-csv'
        )
except:
    pass

try:
    st.header("Dados climáticos")
    uploaded_files = st.file_uploader(
        "Dados climáticos",
        type=['csv'],
        accept_multiple_files=True
    )
    for uploaded_file in uploaded_files:
        # Can be used wherever a "file-like" object is accepted:
        dataframe = pd.read_csv(uploaded_file)

        dataframe_list = dataframe.values.tolist()

        # Calcular o graus dia da mariposa
        graus_dia_mariposa = []
        for item in dataframe_list:
            item.append(item[5] - TEMPERATURA_DE_BASE_MARIPOSA)
            graus_dia_mariposa.append(item[5] - TEMPERATURA_DE_BASE_MARIPOSA)
        # Calcular o graus dia acumulado da mariposa
        for i in range(len(dataframe_list)):
            if (sum(graus_dia_mariposa[:i + 1]) + CONSTANTE_TERMICA_DA_LAGARTA) > CONSTANTE_TERMICA_DA_MARIPOSA:
                dataframe_list[i].append(0)
            else:
                dataframe_list[i].append(
                    sum(graus_dia_mariposa[:i + 1]) + CONSTANTE_TERMICA_DA_LAGARTA)

        # Calcular o graus dia do ovo
        graus_dia_ovo = []
        for i in range(len(dataframe_list)):
            if dataframe_list[i][-1] > 0:
                dataframe_list[i].append(0)
                graus_dia_ovo.append(0)
            else:
                dataframe_list[i].append(
                    dataframe_list[i][5] - TEMPERATURA_DE_BASE_OVO)
                graus_dia_ovo.append(
                    dataframe_list[i][5] - TEMPERATURA_DE_BASE_OVO)

        # Calcular o graus dia do ovo acumulado
        for i in range(len(dataframe_list)):
            if dataframe_list[i][26] > 0:
                a = 0
            else:
                a = sum(graus_dia_ovo[:i + 1])

            if float(a) > CONSTANTE_TERMICA_DO_OVO:
                dataframe_list[i].append("LAGARTA")
            else:
                if dataframe_list[i][26] > 0:
                    dataframe_list[i].append(0)
                else:
                    dataframe_list[i].append(sum(graus_dia_ovo[:i + 1]))

        header = [
            "Faixa de Tempo",
            "mín - Chuva",
            "Chuva",
            "máx - Chuva",
            "mín - Temperatura",
            "Temperatura",
            "máx - Temperatura",
            "mín - Ponto de Orvalho",
            "Ponto de Orvalho",
            "máx - Ponto de Orvalo",
            "mín - Umidade",
            "Umidade",
            "máx - Umidade",
            "mín - Vel, Vento",
            "Vel, Vento",
            "máx - Vel, Vento",
            "mín - Radiação Solar",
            "Radiação Solar",
            "máx - Radiação Solar",
            "mín - Pressão Atm",
            "Pressão Atm",
            "máx - Pressão Atm",
            "mín - Evapotranspiração",
            "Evapotranspiração",
            "máx - Evapotranspiração",
            "Graus dias da Mariposa",
            "Graus dias acumulado da mariposa",
            "Graus dias ovo",
            "Graus dias acumulado do ovo"
        ]

        dataframe = pd.DataFrame(dataframe_list)
        dataframe.columns = header

        with st.expander(f"{uploaded_file.name}"):
            st.write(dataframe)

            data_lagarta = ''
            for i in range(len(dataframe_list)):
                if dataframe_list[i][28] == "LAGARTA":
                    data_lagarta = dataframe_list[i][0]
                    break
                else:
                    data_lagarta = "Não há"

            st.write(f"Data de aparição das lagartas: {data_lagarta}")

            csv = convert_df(dataframe)
            st.download_button(
                "Download {}".format(choice(emojis)),
                csv,
                f"output_{uploaded_file.name}",
                "text/csv",
                key=f'download-csv-{uploaded_file.name}'
            )
except:
    pass
